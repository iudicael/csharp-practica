﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using TicTacToeLibrary;

namespace TicTacToe
{
    public partial class TicTacToe : Form
    {
        private string _player = "";
        private readonly TicTacToeEngine _engine;

        private readonly string[] _buttons =
            {"button1", "button2", "button3", "button4", "button5", "button6", "button7", "button8", "button9"};

        public TicTacToe()
        {
            InitializeComponent();
            Text = @"Tic-Tac-Toe";
            _engine = new TicTacToeEngine();
        }

        public sealed override string Text
        {
            get => base.Text;
            set => base.Text = value;
        }

        private void BepaalWinnaar(TicTacToeEngine engine)
        {
            if (!engine.GameEnds())
            {
                if (engine.Status != GameStatus.Equal) return;
                MessageBox.Show(@"Het is gelijk spel. Er is geen winnaar");
                MaakLeeg();
                return;
            }
            var winner = engine.GetWinner();
            MessageBox.Show($@"Gefeliciteerd! Speler {winner.ToString()} wint. Er is een winnaar!");                
            MaakLeeg();
         }

        private void MaakLeeg()
        {
            button1.Text = "";
            button2.Text = "";
            button3.Text = "";
            button4.Text = "";
            button5.Text = "";
            button6.Text = "";
            button7.Text = "";
            button8.Text = "";
            button9.Text = "";
            _engine.Reset();
        }

        private void button_Click(object sender, EventArgs e)
        {
            _player = _engine.CurrentPlayerMark.ToString();
            if (!(sender is Button pressed)) throw new ArgumentNullException(nameof(pressed));
            if (pressed.Text == "")
            {
                pressed.Text = _player;
                foreach (var button in _buttons)
                {
                    if (pressed.Name != button) continue;
                    int.TryParse(new string(button.Where(x => char.IsDigit(x)).ToArray()), out var cellNumber);
                    _engine.ChooseCell(cellNumber);
                }
            }
            else
            {
                MessageBox.Show(@"Selecteer een ander vak");
            }
            BepaalWinnaar(_engine);
        }
    }
}