﻿using System;
using System.Linq;
using System.Windows.Forms;
using static System.Int32;

namespace Practicum2
{
    public partial class RunnerWindow : Form
    {
        public RunnerWindow()
        {
            InitializeComponent();
            RandomNumbers();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var num1 = Parse(num1Text.Text);
            var num2 = Parse(num2Text.Text);
            var num3 = Parse(num3Text.Text);
            
            var output = MethodRunner.RunAllMethods(num1,num2,num3);
            methodOutput.Text = output;

            output = LambdaRunner.RunAllMethods(num1,num2,num3);
            lambdaOutput.Text = output;
            MessageBox.Show(CheckResult() ? @"Result OK" : @"Result not OK");
        }

        private void RandomNumbers()
        {
            var rnd = new Random();
            num1Text.Text = rnd.Next(1, 100).ToString();
            num2Text.Text = rnd.Next(1, 100).ToString();
            num3Text.Text = rnd.Next(1, 100).ToString();
        }

        private bool CheckResult()
        {
            var method = methodOutput.Text.Split('\n').Select(i => i.Substring(i.IndexOf('=')));
            var lambda = lambdaOutput.Text.Split('\n').Select(i => i.Substring(i.IndexOf('=')));
            var compare = method.Select(s => s.Replace("=", "")).Select(f => lambda.Select(l => l.Replace("=", "")).Count(f.Equals));
            return compare.Sum() == 6;
        }
    }
}
