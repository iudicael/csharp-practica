﻿using System;
using System.Diagnostics;

namespace TicTacToeLibrary
{
    public class TicTacToeEngine
    {
        public GameStatus Status { get; private set; }
        public char CurrentPlayerMark;
        
        private readonly char[,] _gameBoard;
        private int _turn;
        private const int Columns = 3;
        private const int Rows = 3;

        public TicTacToeEngine()
        {
            _gameBoard = new char[Rows,Columns];
            Reset();
        }

        private void InitializeBoard()
        {
            for (var i = 0; i < Rows; i++)
            {
                for (var j = 0; j < Columns; j++)
                {
                    _gameBoard[i, j] = '-';
                }
            }
        }
        
        public string Board()
        {
            return "-------------" +
                   $"\n| {_gameBoard[0,0].ToString()} | {_gameBoard[0,1].ToString()} | {_gameBoard[0,2].ToString()} |" +
                   "\n-------------" +
                   $"\n| {_gameBoard[1,0].ToString()} | {_gameBoard[1,1].ToString()} | {_gameBoard[1,2].ToString()} |" +
                   "\n-------------" +
                   $"\n| {_gameBoard[2,0].ToString()} | {_gameBoard[2,1].ToString()} | {_gameBoard[2,2].ToString()} |" +
                   "\n-------------";
        }

        public bool ChooseCell(int cell)
        {
            if (cell <= 0 || cell > 9) return false;
            var row = ((cell-1) / Rows);
            var col = ((cell-1) % Columns);
            if (_gameBoard[row, col] != '-') return false;
            _gameBoard[row, col] = CurrentPlayerMark;
            _turn++;
            ChangePlayer();
            return true;

        }

        public void Reset()
        {
            InitializeBoard();
            Status = GameStatus.PlayerXPlays;
            CurrentPlayerMark = 'X';
            _turn = 0;
        }

        private void ChangePlayer()
        {
            switch (Status)
            {
                case GameStatus.PlayerOPlays:
                    Status = GameStatus.PlayerXPlays;
                    CurrentPlayerMark = 'X';
                    IsDraw();
                    break;
                case GameStatus.PlayerXPlays:
                    Status = GameStatus.PlayerOPlays;
                    CurrentPlayerMark = 'O';
                    IsDraw();
                    break;
            }
        }

        private void IsDraw()
        {
            if (_turn != 9 || GameEnds()) return;
            Status = GameStatus.Equal;
            CurrentPlayerMark = 'E';
        }

        private void CurrentWinner()
        {
            switch (CurrentPlayerMark)
            {
                case 'O':
                    Status = GameStatus.PlayerXWins;
                    break;
                case 'X':
                    Status = GameStatus.PlayerOWins;
                    break;
                default:
                    Status = GameStatus.Equal;
                    break;
            }
        }

        public char GetWinner()
        {
            switch (Status)
            {
                case GameStatus.PlayerOWins:
                    return 'O';
                case GameStatus.PlayerXWins:
                    return 'X';
                case GameStatus.Equal:
                    return 'E';
            }
            return 'N';
        }

        private static bool CheckRowCol(char c1, char c2, char c3)
        {
            return ((c1 != '-') && (c1 == c2) && (c2 == c3));
        }

        private bool CheckRowsForWin()
        {
            for (var i = 0; i < Rows; i++)
            {
                if (CheckRowCol(_gameBoard[i,0], _gameBoard[i,1], _gameBoard[i,2]))
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckColsForWin()
        {
            for (var i = 0; i < Columns; i++)
            {
                if (CheckRowCol(_gameBoard[0,i], _gameBoard[1,i], _gameBoard[2,i]))
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckDiagonalsForWin()
        {
            return CheckRowCol(_gameBoard[0, 0], _gameBoard[1, 1], _gameBoard[2, 2]) ||
                   CheckRowCol(_gameBoard[0, 2], _gameBoard[1, 1], _gameBoard[2, 0]);
        }

        private bool CheckForWin()
        {
            return CheckRowsForWin() || CheckColsForWin() || CheckDiagonalsForWin();
        }

        public bool GameEnds()
        {
            if (!CheckForWin()) return false;
            CurrentWinner();
            return true;
        }
    }

    public enum GameStatus { PlayerOPlays, PlayerXPlays, Equal, PlayerOWins, PlayerXWins }
}