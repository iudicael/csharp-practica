﻿using System;
using System.Threading;
using TicTacToeLibrary;

namespace TicTacToeConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var engine = new TicTacToeEngine();
            while (true)
            {
                Game(engine);
                Console.WriteLine("Type in a number from 1-9, new or quit\nStatus: " + Status(engine.Status));
                Console.WriteLine(engine.Board());
                var input = Console.ReadLine();
                if (input != null && (input.Equals("new") || input.Equals("n")))
                {
                    engine.Reset();
                    Game(engine);
                }
                else if (input != null && (input.Equals("quit") || input.Equals("q")))
                {
                    break;
                }
            }
            Environment.Exit(0);
        }

        private static void Game(TicTacToeEngine engine)
        {
            while (!engine.GameEnds())
            {
                Console.WriteLine($"Type in a number from 1-9, new (n) or quit (q)\nStatus: {Status(engine.Status)}");
                Console.WriteLine(engine.Board());
                var input = Console.ReadLine();
                if (input != null && (input.Equals("n") || input.Equals("new")))
                {
                    engine.Reset();
                }
                if (input != null && (input.Equals("q") || input.Equals("quit")))
                {
                    Environment.Exit(0);
                }                if (!CheckInput(input))
                {
                    Console.WriteLine("Cell not in range. Chose cell between 0 and 10");
                    input = Console.ReadLine();
                    int.TryParse(input, out var cell);
                    engine.ChooseCell(cell);
                }
                else
                {
                    int.TryParse(input, out var cell);
                    engine.ChooseCell(cell);
                }
            }
        }



        private static bool CheckInput(string input)
        {
            if (input == null) return false;
            int.TryParse(input, out var cell);
            return cell >= 1 && cell <= 9;
        }

        private static string Status(GameStatus status)
        {
            switch (status)
            {
                case GameStatus.PlayerOPlays:
                    return "Player O plays";
                case GameStatus.PlayerXPlays:
                    return "Player X plays";
                case GameStatus.Equal:
                    return "Draw";
                case GameStatus.PlayerOWins:
                    return "Player O wins";
                case GameStatus.PlayerXWins:
                    return "Player X wins";
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }


    }
}
