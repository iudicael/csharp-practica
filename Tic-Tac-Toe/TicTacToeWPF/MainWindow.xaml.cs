﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TicTacToeLibrary;

namespace TicTacToeWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private string _player = "";
        private readonly TicTacToeEngine _engine;

        private readonly string[] _buttons =
            {"Button1", "Button2", "Button3", "Button4", "Button5", "Button6", "Button7", "Button8", "Button9"};
        
        public MainWindow()
        {
            InitializeComponent();
            Title = @"Tic-Tac-Toe";
            _engine = new TicTacToeEngine();
        }
        
        private void BepaalWinnaar(TicTacToeEngine engine)
        {
            if (!engine.GameEnds())
            {
                if (engine.Status != GameStatus.Equal) return;
                MessageBox.Show(@"Het is gelijk spel. Er is geen winnaar");
                MaakLeeg();
                _engine.Reset();
                return;
            }
            var winner = engine.GetWinner();
            MessageBox.Show($@"Gefeliciteerd! Speler {winner.ToString()} wint. Er is een winnaar!");                
            _engine.Reset();
            MaakLeeg();
        }
        
        private void MaakLeeg()
        {
            Button1.Content = "";
            Button2.Content = "";
            Button3.Content = "";
            Button4.Content = "";
            Button5.Content = "";
            Button6.Content = "";
            Button7.Content = "";
            Button8.Content = "";
            Button9.Content = "";
        }

        private void gameAction_OnClick(object sender, RoutedEventArgs e)
        {
            _player = _engine.CurrentPlayerMark.ToString();
            var cell = true;
            if (!(sender is Button pressed)) throw new ArgumentNullException(nameof(pressed));
            if ((string) pressed.Content == "")
            {
                pressed.Content = _player;
                foreach (var button in _buttons)
                {
                    if (pressed.Name != button) continue;
                    int.TryParse(new string(button.Where(char.IsDigit).ToArray()), out var cellNumber);
                    cell = _engine.ChooseCell(cellNumber);
                }
            }
            if (!cell)
            {
                MessageBox.Show(@"Selecteer een ander vak");
            }
            BepaalWinnaar(_engine);
        }
    }
}